
package model;

public class Ambiente {
    private String campo;
    private String ecologico;
    private String serraMontanha;
    private String termas;
    private String praia;

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getEcologico() {
        return ecologico;
    }

    public void setEcologico(String ecologico) {
        this.ecologico = ecologico;
    }

    public String getSerraMontanha() {
        return serraMontanha;
    }

    public void setSerraMontanha(String serraMontanha) {
        this.serraMontanha = serraMontanha;
    }

    public String getTermas() {
        return termas;
    }

    public void setTermas(String termas) {
        this.termas = termas;
    }

    public String getPraia() {
        return praia;
    }

    public void setPraia(String praia) {
        this.praia = praia;
    }
    
    
}
