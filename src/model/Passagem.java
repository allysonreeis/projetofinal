
package model;

import java.util.ArrayList;

public class Passagem {
    private String aerea; //Podem ser objetos
    private String terrestre;
    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getAerea() {
        return aerea;
    }

    public void setAerea(String aerea) {
        this.aerea = aerea;
    }

    public String getTerrestre() {
        return terrestre;
    }

    public void setTerrestre(String terrestre) {
        this.terrestre = terrestre;
    }
    
    
}
