
package controller;

import java.util.ArrayList;
import model.Administrador;

public class ControleAdministrador {
    
    private ArrayList<Administrador> listaAdministrador;
    
    public ControleAdministrador () {
        this.listaAdministrador = new ArrayList<Administrador>();
    }

    public ArrayList<Administrador> getListaAdministrador() {
        return listaAdministrador;
    }

    public void setListaAdministrador(ArrayList<Administrador> listaAdministrador) {
        this.listaAdministrador = listaAdministrador;
    }
    
    public void adicionarAdministrador (Administrador umAdministrador) {
        this.listaAdministrador.add(umAdministrador);
    }
    
    public void removerAdministrador (Administrador umAdministrador) {
        this.listaAdministrador.remove(umAdministrador);
    }
    
    public Administrador pesquisarAdministrador (String umNome) {
        for (Administrador umAdministrador : listaAdministrador) {
            if (umAdministrador.getNome().equalsIgnoreCase(umNome)) {
                return umAdministrador;
            }
        }
        return null;
    }
    
    public Administrador pesquisarAdministrador (String email, String senha) {
        for (Administrador umAdministrador : listaAdministrador) {
            if (umAdministrador.getEmail().equals(email) && umAdministrador.getSenha().equals(senha)) {
                return umAdministrador;
            }
        }
        return null;
    }
    
    public Administrador pesquisarAdministrador (String email, String senha, String codigo) {
        for (Administrador umAdministrador : listaAdministrador) {
            if (umAdministrador.getEmail().equals(email) && umAdministrador.getSenha().equals(senha)
                    && umAdministrador.getNumeroCadastro().equals(codigo)) {
                return umAdministrador;
            }
        }
        return null;
    }
    
    public void validarCadastro (String umNome, String umCpf, String umRg) {
        /*verificar se existe caracteres invalidos*/
    }
    
    public void enviarSolicitacao () {
        /*envia os dados para o gerente do hotel*/
    }
    
    public void cobrarMulta (/*recebe a data atual para verificar a multa*/) {
        
    }
}
