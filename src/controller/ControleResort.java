
package controller;

import java.util.ArrayList;
import model.Resort;

public class ControleResort {
    private ArrayList<Resort> listaResorts;

    public ArrayList<Resort> getListaResorts() {
        return listaResorts;
    }

    public void setListaResorts(ArrayList<Resort> listaResorts) {
        this.listaResorts = listaResorts;
    }
    
    public void adicionarResort (Resort umResort) {
        this.listaResorts.add(umResort);
    }
    
    public void removerResort (Resort umResort) {
        this.listaResorts.remove(umResort);
    }
    
    public Resort pesquisarResort (String umNome, String umCep) {
        for (Resort umResort : listaResorts) {
            if (umResort.getNome().equalsIgnoreCase(umNome) && 
                    umResort.getCep().equalsIgnoreCase(umCep)) {
                return umResort;
            }
        }
        return null;
    }
}
