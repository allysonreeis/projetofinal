
package GUI;

import controller.ControleAdministrador;
import controller.ControleCliente;
import controller.ControleHospedagem;
import controller.ControleHotel;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import model.Administrador;
import model.Cliente;
import model.Endereco;

/**
 *
 * @author reis
 */
public class Login extends javax.swing.JFrame {

    
    private Administrador umAdministrador;
    private boolean novaPessoa;
    private boolean novoAdministrador;
    private ArrayList<String> umaListaTelefones;
    private Cliente umCliente;
    private DefaultListModel telefonesListModel;
    private ControleCliente umControleCliente;
    private ControleAdministrador umControleAdministrador;
    private ControleHotel umControleHotel;
    private ControleHospedagem umControleHospedagem;
    
    public Login() {
        initComponents();
        this.jPasswordFieldSenhaCadastro.setEchoChar('\u0000');
        this.jPasswordFieldConfirmaSenha.setEchoChar('\u0000');
        this.telefonesListModel = new DefaultListModel();
        this.jListTelefoneCadastro.setModel(telefonesListModel);
        this.umControleCliente = new ControleCliente();
        this.umControleAdministrador = new ControleAdministrador();
        this.umControleHotel = new ControleHotel();
        this.umControleHospedagem = new ControleHospedagem();
        this.novaPessoa = true;
        this.novoAdministrador = true;
    }
    
    public void limparCampos () {
        jTextFieldEmailCadastro.setFont(new java.awt.Font("Waree", 0, 15));
        jTextFieldEmailCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldEmailCadastro.setText("Email");
        
        jTextFieldConfirmaEmail.setFont(new java.awt.Font("Waree", 0, 15)); 
        jTextFieldConfirmaEmail.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldConfirmaEmail.setText("Confirme o email");
        
        jTextFieldNomeCadastro.setFont(new java.awt.Font("Waree", 0, 15)); 
        jTextFieldNomeCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldNomeCadastro.setText("Nome Completo");
        
        jPasswordFieldSenhaCadastro.setFont(new java.awt.Font("Waree", 0, 15)); 
        jPasswordFieldSenhaCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jPasswordFieldSenhaCadastro.setText("Senha");
        jPasswordFieldSenhaCadastro.setEchoChar('\u0000');
        
        jPasswordFieldConfirmaSenha.setFont(new java.awt.Font("Waree", 0, 15)); 
        jPasswordFieldConfirmaSenha.setForeground(new java.awt.Color(113, 113, 113));
        jPasswordFieldConfirmaSenha.setText("Confirme a Senha");
        jPasswordFieldConfirmaSenha.setEchoChar('\u0000');
        
        jTextFieldNumeroCadastro.setFont(new java.awt.Font("Waree", 0, 15));
        jTextFieldNumeroCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldNumeroCadastro.setText("Número de Cadastro");
        
        jFormattedTextFieldDataNascimento.setText(null);
        telefonesListModel.clear();
    }
    
    public void salvarCadastroAdministrador () {
        String senha = new String (jPasswordFieldSenhaCadastro.getPassword());
        ArrayList<String> telefones;
        
        telefones = new ArrayList<String>();
        for (int i = 0; i < telefonesListModel.size(); i++) {
            telefones.add(telefonesListModel.getElementAt(i).toString());
        }
        this.umaListaTelefones = telefones;
        
        if (this.novaPessoa == true) {
            this.umAdministrador = new Administrador(jTextFieldNomeCadastro.getText(), jTextFieldEmailCadastro.getText(), 
                    senha, this.umaListaTelefones, jTextFieldNumeroCadastro.getText());
        } else {
            this.umAdministrador.setEmail(jTextFieldEmailCadastro.getText());
            this.umAdministrador.setNome(jTextFieldNomeCadastro.getText());
            this.umAdministrador.setSenha(senha);
            this.umAdministrador.setListaTelefones(umaListaTelefones);
            this.umAdministrador.setNumeroCadastro(jTextFieldNumeroCadastro.getText());
            this.umAdministrador.setDataNascimento(jFormattedTextFieldDataNascimento.getText());
        }
        
        if (this.novoAdministrador == true) {
            this.umControleAdministrador.adicionarAdministrador(umAdministrador);
        }
        
        this.novoAdministrador = false;
    }
    
    public void salvarCadastroUsuario () {
        String senha = new String(jPasswordFieldSenhaCadastro.getPassword());
        ArrayList<String> telefones;
        
        telefones = new ArrayList<String>();
        for (int i = 0; i < telefonesListModel.size(); i++) {
            telefones.add(telefonesListModel.getElementAt(i).toString());
        }
        
        this.umaListaTelefones = telefones;
        
        if (this.novaPessoa == true) {
            this.umCliente = new Cliente(jTextFieldNomeCadastro.getText(), jTextFieldEmailCadastro.getText(),
                   senha, umaListaTelefones);
        } else {
            this.umCliente.setEmail(jTextFieldEmailCadastro.getText());
            this.umCliente.setNome(jTextFieldNomeCadastro.getText());
            this.umCliente.setSenha(senha);
            this.umCliente.setListaTelefones(umaListaTelefones);
        }
        
        if (this.novaPessoa == true) {
            this.umControleCliente.adicionarCliente(umCliente);
        }
        
        this.novaPessoa = false;
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelCadastro = new javax.swing.JLabel();
        jTextFieldEmailCadastro = new javax.swing.JTextField();
        jTextFieldConfirmaEmail = new javax.swing.JTextField();
        jTextFieldNomeCadastro = new javax.swing.JTextField();
        jPasswordFieldSenhaCadastro = new javax.swing.JPasswordField();
        jButtonSalvar = new javax.swing.JButton();
        jPasswordFieldConfirmaSenha = new javax.swing.JPasswordField();
        jFormattedTextFieldDataNascimento = new javax.swing.JFormattedTextField();
        jLabelDataNascimento = new javax.swing.JLabel();
        jLabelTelefoneCadastro = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListTelefoneCadastro = new javax.swing.JList();
        jButtonAdicionarTelCadastro = new javax.swing.JButton();
        jButtonRemoverTelCadastro = new javax.swing.JButton();
        jTextFieldNumeroCadastro = new javax.swing.JTextField();
        jButtonCadastrarAdministrador = new javax.swing.JButton();
        jLabelLogin = new javax.swing.JLabel();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldEmailLogin = new javax.swing.JTextField();
        jLabelSenha = new javax.swing.JLabel();
        jButtonEntrar = new javax.swing.JButton();
        jPasswordFieldSenhaLogin = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabelCadastro.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabelCadastro.setText("Cadastro");

        jTextFieldEmailCadastro.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jTextFieldEmailCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldEmailCadastro.setText("Email");
        jTextFieldEmailCadastro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldEmailCadastroKeyTyped(evt);
            }
        });

        jTextFieldConfirmaEmail.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jTextFieldConfirmaEmail.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldConfirmaEmail.setText("Confirme o email");
        jTextFieldConfirmaEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldConfirmaEmailKeyTyped(evt);
            }
        });

        jTextFieldNomeCadastro.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jTextFieldNomeCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldNomeCadastro.setText("Nome Completo");
        jTextFieldNomeCadastro.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextFieldNomeCadastro.setName(""); // NOI18N
        jTextFieldNomeCadastro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNomeCadastroKeyTyped(evt);
            }
        });

        jPasswordFieldSenhaCadastro.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jPasswordFieldSenhaCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jPasswordFieldSenhaCadastro.setText("Senha");
        jPasswordFieldSenhaCadastro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPasswordFieldSenhaCadastroKeyTyped(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jPasswordFieldConfirmaSenha.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jPasswordFieldConfirmaSenha.setForeground(new java.awt.Color(113, 113, 113));
        jPasswordFieldConfirmaSenha.setText("Confirme a Senha");
        jPasswordFieldConfirmaSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jPasswordFieldConfirmaSenhaKeyTyped(evt);
            }
        });

        try {
            jFormattedTextFieldDataNascimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabelDataNascimento.setForeground(new java.awt.Color(113, 113, 113));
        jLabelDataNascimento.setText("Data de Nascimento");

        jLabelTelefoneCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jLabelTelefoneCadastro.setText("Telefone");

        jScrollPane1.setViewportView(jListTelefoneCadastro);

        jButtonAdicionarTelCadastro.setText("+");
        jButtonAdicionarTelCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarTelCadastroActionPerformed(evt);
            }
        });

        jButtonRemoverTelCadastro.setText("-");

        jTextFieldNumeroCadastro.setEditable(false);
        jTextFieldNumeroCadastro.setFont(new java.awt.Font("Waree", 0, 15)); // NOI18N
        jTextFieldNumeroCadastro.setForeground(new java.awt.Color(113, 113, 113));
        jTextFieldNumeroCadastro.setText("Número de Cadastro");
        jTextFieldNumeroCadastro.setToolTipText("");

        jButtonCadastrarAdministrador.setText("Cadastrar Administrador");
        jButtonCadastrarAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCadastrarAdministradorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTextFieldNomeCadastro)
                        .addComponent(jLabelCadastro)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jTextFieldEmailCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jTextFieldConfirmaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jPasswordFieldSenhaCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jPasswordFieldConfirmaSenha))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jButtonCadastrarAdministrador))
                        .addComponent(jLabelTelefoneCadastro))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jFormattedTextFieldDataNascimento, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelDataNascimento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextFieldNumeroCadastro, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonRemoverTelCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonAdicionarTelCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelCadastro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextFieldNomeCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEmailCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldConfirmaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPasswordFieldSenhaCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPasswordFieldConfirmaSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelDataNascimento)
                .addGap(12, 12, 12)
                .addComponent(jFormattedTextFieldDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelTelefoneCadastro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAdicionarTelCadastro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonRemoverTelCadastro)))
                .addGap(18, 18, 18)
                .addComponent(jTextFieldNumeroCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvar)
                    .addComponent(jButtonCadastrarAdministrador))
                .addGap(6, 6, 6))
        );

        jTextFieldNomeCadastro.getAccessibleContext().setAccessibleName("");

        jLabelLogin.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabelLogin.setText("Login");

        jLabelNome.setText("Email");

        jLabelSenha.setText("Senha");

        jButtonEntrar.setText("Entrar");
        jButtonEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEntrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelSenha)
                        .addGap(18, 18, 18)
                        .addComponent(jPasswordFieldSenhaLogin, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonEntrar)
                            .addComponent(jLabelLogin))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelNome)
                        .addGap(24, 24, 24)
                        .addComponent(jTextFieldEmailLogin)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabelLogin)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldEmailLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSenha)
                    .addComponent(jPasswordFieldSenhaLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButtonEntrar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldNomeCadastroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNomeCadastroKeyTyped
        if (this.jTextFieldNomeCadastro.getText().equals("Nome Completo")) {
            this.jTextFieldNomeCadastro.setText(null);
            this.jTextFieldNomeCadastro.setForeground(Color.black);
        } else if (this.jTextFieldNomeCadastro.getText().isEmpty()) {
            this.jTextFieldNomeCadastro.setForeground(new java.awt.Color(113, 113, 113));
            this.jTextFieldNomeCadastro.setText("Nome Completo");
            this.jTextFieldNomeCadastro.setCaretPosition(0);
        }
    }//GEN-LAST:event_jTextFieldNomeCadastroKeyTyped

    private void jPasswordFieldSenhaCadastroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordFieldSenhaCadastroKeyTyped
       String senha = new String(this.jPasswordFieldSenhaCadastro.getPassword());
       if (senha.equalsIgnoreCase("Senha")) {
           this.jPasswordFieldSenhaCadastro.setEchoChar('\u2022');
           this.jPasswordFieldSenhaCadastro.setText(null);
       } else if (senha.isEmpty()) {
           this.jPasswordFieldSenhaCadastro.setEchoChar('\u0000');
           this.jPasswordFieldSenhaCadastro.setText("Senha");
       }
    }//GEN-LAST:event_jPasswordFieldSenhaCadastroKeyTyped

    private void jPasswordFieldConfirmaSenhaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordFieldConfirmaSenhaKeyTyped
       String senha = new String(this.jPasswordFieldConfirmaSenha.getPassword());
       if (senha.equalsIgnoreCase("Confirme a Senha")) {
           this.jPasswordFieldConfirmaSenha.setEchoChar('\u2022');
           this.jPasswordFieldConfirmaSenha.setText(null);
       } else if (senha.isEmpty()) {
           this.jPasswordFieldConfirmaSenha.setEchoChar('\u0000');
           this.jPasswordFieldConfirmaSenha.setText("Confirme a Senha");
           this.jPasswordFieldConfirmaSenha.setCaretPosition(0);
       }
    }//GEN-LAST:event_jPasswordFieldConfirmaSenhaKeyTyped

    private void jButtonEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEntrarActionPerformed

        String senha = new String (jPasswordFieldSenhaLogin.getPassword());
        
        this.umCliente = this.umControleCliente.pesquisarCliente(jTextFieldEmailLogin.getText(), senha);
        this.umAdministrador = this.umControleAdministrador.pesquisarAdministrador(jTextFieldEmailLogin.getText(),
                senha);
        
        if (this.umCliente == null && this.umAdministrador == null) {
            this.exibirInformacao("Os dados não conferem");
        } else if (this.umCliente != null && this.umAdministrador == null) {
            Home home = new Home(this, true, umCliente, null, umControleHotel, umControleHospedagem);
            home.setVisible(true);
            umControleHotel = home.getUmControleHotel();
            umControleHospedagem = home.getUmControleHospedagem();
            
            jTextFieldEmailLogin.setText(null);
            jPasswordFieldSenhaLogin.setText(null);
        } else if (this.umCliente == null && this.umAdministrador != null) {
            String pesquisa = JOptionPane.showInputDialog("Informe o número de cadastro.");
            this.umAdministrador = this.umControleAdministrador.pesquisarAdministrador(
                    jTextFieldEmailLogin.getText(), senha, pesquisa);
            if (umAdministrador == null) {
                exibirInformacao("Código não confere!");
            } else {
                Home home = new Home(this, true, null, umAdministrador, umControleHotel, umControleHospedagem);
                home.setVisible(true);
                umControleHotel = home.getUmControleHotel();
                umControleHospedagem = home.getUmControleHospedagem();
                
                jTextFieldEmailLogin.setText(null);
                jPasswordFieldSenhaLogin.setText(null);
            }
        }
    }//GEN-LAST:event_jButtonEntrarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        String senha = new String (this.jPasswordFieldSenhaCadastro.getPassword());
        String confirmaSenha = new String (this.jPasswordFieldConfirmaSenha.getPassword());
        this.novaPessoa = true;
        if (this.jTextFieldEmailCadastro.getText().equals(this.jTextFieldConfirmaEmail.getText()) == false){
            this.exibirInformacao("O email não confere!");
        } else if (senha.equals(confirmaSenha) == false) {
            this.exibirInformacao("A senha não confere!");
        } else {
            if (this.jTextFieldNumeroCadastro.getText().equals("Número de Cadastro") == true
                        || this.jTextFieldNumeroCadastro.getText().equals(null) == true) {
                this.salvarCadastroUsuario();
            } else {
                this.salvarCadastroAdministrador();
            }
            this.limparCampos();
        }
        jTextFieldEmailLogin.requestFocus();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCadastrarAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCadastrarAdministradorActionPerformed
        String pesquisa = JOptionPane.showInputDialog("Informe o número de cadastro.");
        if (pesquisa != null) {
            this.jTextFieldNumeroCadastro.setText(pesquisa);
        }
    }//GEN-LAST:event_jButtonCadastrarAdministradorActionPerformed

    private void jButtonAdicionarTelCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarTelCadastroActionPerformed
        CadastroTelefone cadastro = new CadastroTelefone(this, true);
        cadastro.setVisible(true);
        if (cadastro.getTelefone() != null) {
            telefonesListModel.addElement(cadastro.getTelefone());
        }
        cadastro.dispose();
    }//GEN-LAST:event_jButtonAdicionarTelCadastroActionPerformed

    private void jTextFieldEmailCadastroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldEmailCadastroKeyTyped
        if (this.jTextFieldEmailCadastro.getText().equals("Email")) {
            this.jTextFieldEmailCadastro.setText(null);
            this.jTextFieldEmailCadastro.setForeground(Color.black);
        } else if (this.jTextFieldEmailCadastro.getText().isEmpty()) {
            this.jTextFieldEmailCadastro.setForeground(new java.awt.Color(113, 113, 113));
            this.jTextFieldEmailCadastro.setText("Email");
            this.jTextFieldEmailCadastro.setCaretPosition(0);
        }
    }//GEN-LAST:event_jTextFieldEmailCadastroKeyTyped

    private void jTextFieldConfirmaEmailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldConfirmaEmailKeyTyped
        if (this.jTextFieldConfirmaEmail.getText().equals("Confirme o email")) {
            this.jTextFieldConfirmaEmail.setText(null);
            this.jTextFieldConfirmaEmail.setForeground(Color.black);
        } else if (this.jTextFieldConfirmaEmail.getText().isEmpty()) {
            this.jTextFieldConfirmaEmail.setForeground(new java.awt.Color(113, 113, 113));
            this.jTextFieldConfirmaEmail.setText("Confirme o email");
            this.jTextFieldConfirmaEmail.setCaretPosition(0);
        }
    }//GEN-LAST:event_jTextFieldConfirmaEmailKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarTelCadastro;
    private javax.swing.JButton jButtonCadastrarAdministrador;
    private javax.swing.JButton jButtonEntrar;
    private javax.swing.JButton jButtonRemoverTelCadastro;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JFormattedTextField jFormattedTextFieldDataNascimento;
    private javax.swing.JLabel jLabelCadastro;
    private javax.swing.JLabel jLabelDataNascimento;
    private javax.swing.JLabel jLabelLogin;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelSenha;
    private javax.swing.JLabel jLabelTelefoneCadastro;
    private javax.swing.JList jListTelefoneCadastro;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField jPasswordFieldConfirmaSenha;
    private javax.swing.JPasswordField jPasswordFieldSenhaCadastro;
    private javax.swing.JPasswordField jPasswordFieldSenhaLogin;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextFieldConfirmaEmail;
    private javax.swing.JTextField jTextFieldEmailCadastro;
    private javax.swing.JTextField jTextFieldEmailLogin;
    private javax.swing.JTextField jTextFieldNomeCadastro;
    private javax.swing.JTextField jTextFieldNumeroCadastro;
    // End of variables declaration//GEN-END:variables
}
